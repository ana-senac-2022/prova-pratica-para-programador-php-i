<?php

include 'banco.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $id = $_POST['id'];
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $comissao = $_POST['comissao'];
    $valorvenda = $_POST['valor_venda'];
    $datavenda = $_POST['data_venda'];

    $comissao = calcularComissao($valorVenda);

    $venda = array(
        'id' => $id,
        'nome' => $nome,
        'email' => $email,
        'comissão' => $comissao,
        'valorvenda' => $valorvenda,
        'datavenda' => $datavenda
    );

    $vendas[] = $venda;
}

try {
    $conn = new PDO("mysql:host=$localhost;dbname=$crud_vendedores", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $id = $_POST['id'];

    $sql = "SELECT vendedores.id, vendedores.nome, vendedores.email, vendas.comissao, vendas.valor_venda, vendas.data_venda 
            FROM vendedores 
            INNER JOIN vendas ON vendedores.id = vendas.id_vendedor 
            WHERE vendedores.id = :id_vendedor";

    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':id_vendedor', $id_vendedor);
    $stmt->execute();

    echo "<table>";
    echo "<tr><th>ID</th><th>Nome</th><th>Email</th><th>Comissão</th><th>Valor da Venda</th><th>Data da Venda</th></tr>";

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr>";
        echo "<td>{$row['id']}</td>";
        echo "<td>{$row['nome']}</td>";
        echo "<td>{$row['email']}</td>";
        echo "<td>{$row['comissao']}</td>";
        echo "<td>{$row['valor_venda']}</td>";
        echo "<td>{$row['data_venda']}</td>";
        echo "</tr>";
    }

    echo "</table>";
} catch (PDOException $e) {
    echo "Erro: " . $e->getMessage();
}
