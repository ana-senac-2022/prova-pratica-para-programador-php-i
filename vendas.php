<?php
include 'banco.php';

$host = "localhost";
$dbname = "crud_vendedores";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $id_vendedor = $_POST['id_vendedor'];
    $valor_venda = $_POST['valor_venda'];
    $comissao = $_POST['comissao'];
    $data_venda = $_POST['data_venda'];

    $sql = "INSERT INTO vendas (id_vendedor, valor_venda, comissao, data_venda) VALUES (:id_vendedor, :valor_venda, :comissao, :data_venda)";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':id_vendedor', $id_vendedor);
    $stmt->bindParam(':valor_venda', $valor_venda);
    $stmt->bindParam(':comissao', $comissao);
    $stmt->bindParam(':data_venda', $data_venda);
    $stmt->execute();

    echo "Venda inserida com sucesso!";
} catch (PDOException $e) {
    echo "Erro: " . $e->getMessage();
}
