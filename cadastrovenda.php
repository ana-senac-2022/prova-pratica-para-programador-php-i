<!DOCTYPE html>
<html>

<head>
    <title>Cadastro de Vendas</title>
</head>

<body>
    <h1>Cadastro de Vendas</h1>

    <?php

    $vendas = array();

    function calcularComissao($valorVenda)
    {
        $taxaComissao = 8.5;
        return $valorVenda * $taxaComissao;
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        $vendedor = $_POST['vendedor'];
        $valorVenda = $_POST['valor_venda'];

        $comissao = calcularComissao($valorVenda);

        $venda = array(
            'vendedor' => $vendedor,
            'valor_venda' => $valorVenda,
            'comissao' => $comissao
        );

        $vendas[] = $venda;
    }
    ?>

    <form method="POST" action="">
        <label for="vendedor">Vendedor:</label>
        <input type="text" id="vendedor" name="vendedor" required><br>

        <label for="valor_venda">Valor da Venda:</label>
        <input type="number" id="valor_venda" name="valor_venda" required><br>

        <input type="submit" value="Cadastrar Venda">
    </form>

    <h2>Vendas Registradas</h2>
    <table border="1">
        <tr>
            <th>Vendedor</th>
            <th>Valor da Venda</th>
            <th>Comissão</th>
        </tr>
        <?php foreach ($vendas as $venda) : ?>
            <tr>
                <td><?php echo $venda['vendedor']; ?></td>
                <td>R$ <?php echo number_format($venda['valor_venda'], 2); ?></td>
                <td>R$ <?php echo number_format($venda['comissao'], 2); ?></td>
            </tr>
        <?php endforeach; ?>
    </table>
</body>

</html>